;;;; This file was automatically generated by /home/bill/programming/cl-glfw/generators/make-bindings-from-spec.lisp

(in-package #:cl-glfw-opengl) 

;;;; ext_texture_perturb_normal

(defconstant +perturb-ext+ #x85AE) 
(defconstant +texture-normal-ext+ #x85AF) 
(defglextfun "TextureNormalEXT" texture-normal-ext :return "void" :args
 ((:name |mode| :type |TextureNormalModeEXT| :direction :in)) :category
 "EXT_texture_perturb_normal" :version "1.1") 
